FROM python:3.9.13-slim-buster

RUN apt-get update && apt-get install -y \
     libsodium23 \
     gnupg2 \
     tini \
    build-essential \
    libpq-dev \
    netcat \
   && rm -rf /var/lib/apt/lists/*

WORKDIR /user-rest-api
COPY requirements.txt /user-rest-api/
RUN pip install -r requirements.txt

COPY . /user-rest-api/

ENTRYPOINT ["./django-entrypoint.sh"]
