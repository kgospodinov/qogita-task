# User API
Restful API for managing addresses


### Instructions
The application has been dockerized for ease of use.
### Running the docker image
Clone the repository.
```
git clone https://gitlab.com/kgospodinov/qogita-task.git
```
Build the  image
```
cd qogita-task
docker-compose build
```
Run the Django App as well as the PostgreSQL
```
docker-compose up
```
The application can then be accessed at http://localhost:8000.
A test user has been created with the following credentials:
username: ```test_user```
password: ```password```

### Running tests inside the container
```docker exec -it <container_id> python manage.py test```

### Developing Locally
Python 3.8 is required.

Install the project requirements
``pip install -r requirements.txt``

Configure the database options in settings.py file.
```
DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql",
        "NAME": "POSTGRES_NAME",
        "USER": "POSTGRES_USER",
        "PASSWORD": "POSTGRES_PASSWORD",
        "HOST": "POSTGRES_HOST",
        "PORT": "POSTGRES_PORT",
    }
}
```
or if you prefer using SQLite
```
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': 'mydatabase',
    }
}
```
Run the migrations:
```
 ./manage migrate
 ```
Run the dev server:
```
./manage.py runserver 0.0.0.0:8000
```
The application can then be accessed at http://localhost:8000
You can create the test user with:
```
 ./manage create_test_user
 ```
Running integration tests
```./manage test```

# API endpoint documentation
[OpenAPI Specification](https://gitlab.com/kgospodinov/qogita-task/-/blob/master/openapi.yaml)
