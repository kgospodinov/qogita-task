from django.contrib.auth.models import User
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = "Creates a test user"

    def handle(self, *args, **options):
        if not User.objects.filter(username="test_user").exists():
            user = User.objects.create_user(username="test_user", password="password", email="user@example.com")
            self.stdout.write(self.style.SUCCESS('Successfully created test user with username "%s"' % user.username))
