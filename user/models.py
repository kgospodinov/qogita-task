from django.contrib.auth.models import User
from django.db import models
from django_countries.fields import CountryField


class UserAddressModel(models.Model):
    class Meta:
        db_table = "user_address"
        unique_together = ["user", "address_1", "address_2", "post_code", "city", "country_code"]

    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="addresses")
    updated_at = models.DateTimeField(auto_now=True)
    address_1 = models.CharField(max_length=46, db_index=True)
    address_2 = models.CharField(max_length=46, null=False, blank=True, db_index=True)
    post_code = models.CharField(max_length=10, db_index=True)
    city = models.CharField(max_length=50, db_index=True)
    country_code = CountryField(db_index=True)
