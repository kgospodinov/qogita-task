from django_countries import countries
from django_countries.serializer_fields import CountryField
from rest_framework import serializers
from rest_framework.validators import UniqueTogetherValidator

from user.models import UserAddressModel


class UserAddressSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserAddressModel
        fields = "__all__"
        read_only_fields = ["id", "updated_at"]
        validators = [
            UniqueTogetherValidator(
                queryset=UserAddressModel.objects.all(),
                fields=["user", "address_1", "address_2", "post_code", "city", "country_code"],
            )
        ]

    user = serializers.HiddenField(default=serializers.CurrentUserDefault())
    country_code = CountryField()

    def get_country_name(self, user_address):
        return dict(countries)[user_address.country_code]


class UserAddresFilterSerializer(serializers.Serializer):
    address_1 = serializers.CharField(required=False)
    address_2 = serializers.CharField(required=False)
    post_code = serializers.CharField(required=False)
    city = serializers.CharField(required=False)
    country_code = CountryField(required=False)


class UserAddressBulkDeleteSerializer(serializers.Serializer):
    address_ids = serializers.ListField(allow_empty=False, child=serializers.IntegerField())
