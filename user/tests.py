from datetime import datetime

from django.contrib.auth.models import User
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient
from rest_framework.test import APITestCase
from rest_framework_simplejwt.tokens import RefreshToken

from user.models import UserAddressModel
from user.serializers import UserAddressSerializer


class UserAddressViewSetTestCase(APITestCase):
    def setUp(self):
        self.user = User.objects.create_user(username="user", password="password", email="user@example.com")
        self.client = APIClient()
        self.authenticated_client = APIClient()
        self.authenticated_client.credentials(HTTP_AUTHORIZATION=f"Bearer {self.get_jwt_access_token(self.user)}")

    @staticmethod
    def get_jwt_access_token(user):
        return RefreshToken.for_user(user).access_token

    def test_detail_returns_401_on_get_if_no_token_provided(self):
        response = self.client.get(reverse("user:address-detail", args=[1]))
        self.assertEqual(status.HTTP_401_UNAUTHORIZED, response.status_code)

    def test_detail_returns_200_on_get(self):
        address = self.user.addresses.create(
            address_1="10 Downing Street",
            post_code="SW1A 2AA",
            city="London",
            country_code="UK",
        )
        response = self.authenticated_client.get(path=reverse("user:address-detail", args=[address.id]))
        self.assertEqual(status.HTTP_200_OK, response.status_code)

    def test_detail_returns_404_on_get_if_no_address_found(self):
        response = self.authenticated_client.get(path=reverse("user:address-detail", args=[1]))
        self.assertEqual(status.HTTP_404_NOT_FOUND, response.status_code)

    def test_detail_returns_correct_address_on_get(self):
        address = self.user.addresses.create(
            address_1="10 Downing Street",
            post_code="SW1A 2AA",
            city="London",
            country_code="GB",
        )
        response = self.authenticated_client.get(reverse("user:address-detail", args=[address.id]))
        expected_data = UserAddressSerializer(address).data
        self.assertEqual(expected_data, response.json())

    def test_list_returns_correct_address_count_on_get(self):
        self.user.addresses.create(
            address_1="10 Downing Street",
            post_code="SW1A 2AA",
            city="London",
            country_code="GB",
        )
        self.user.addresses.create(
            address_1="101 Downing Street",
            post_code="SW1A 2AA",
            city="London",
            country_code="GB",
        )
        self.authenticated_client.get(reverse("user:address-list"))
        self.assertEqual(2, UserAddressModel.objects.count())

    def test_detail_returns_401_on_post_if_no_token_provided(self):
        response = self.client.post(path=reverse("user:address-detail", args=[1]), data={})
        self.assertEqual(status.HTTP_401_UNAUTHORIZED, response.status_code)

    def test_list_returns_201_on_post(self):
        response = self.authenticated_client.post(
            path=reverse("user:address-list"),
            data={
                "address_1": "10 Downing Street",
                "address_2": "",
                "post_code": "10",
                "city": "10 Downing Street",
                "country_code": "GB",
            },
        )
        self.assertEqual(status.HTTP_201_CREATED, response.status_code)

    def test_list_creates_record_on_post(self):
        response = self.authenticated_client.post(
            path=reverse("user:address-list"),
            data={
                "address_1": "10 Downing Street",
                "address_2": "",
                "post_code": "10",
                "city": "10 Downing Street",
                "country_code": "GB",
            },
        )
        self.assertIsNotNone(UserAddressModel.objects.get(pk=response.json()["id"]))

    def test_list_returns_400_on_post_if_address_is_duplicated(self):
        self.authenticated_client.post(
            path=reverse("user:address-list"),
            data={
                "address_1": "10 Downing Street",
                "address_2": "",
                "post_code": "10",
                "city": "10 Downing Street",
                "country_code": "GB",
            },
        )
        response = self.authenticated_client.post(
            path=reverse("user:address-list"),
            data={
                "address_1": "10 Downing Street",
                "address_2": "",
                "post_code": "10",
                "city": "10 Downing Street",
                "country_code": "GB",
            },
        )
        self.assertEqual(status.HTTP_400_BAD_REQUEST, response.status_code)

    def test_detail_returns_updated_address_on_put(self):
        address = self.user.addresses.create(
            address_1="10 Downing Street",
            post_code="SW1A 2AA",
            city="London",
            country_code="GB",
        )
        self.authenticated_client.put(
            path=reverse("user:address-detail", args=[address.id]),
            data={
                "address_1": "10 Downing Street",
                "address_2": "",
                "post_code": "test",
                "city": "10 Downing Street",
                "country_code": "GB",
            },
        )
        address.refresh_from_db()
        self.assertEqual("test", address.post_code)

    def test_detail_returns_404_on_put_if_not_address_owner(self):
        other_user = User.objects.create_user(username="user1", password="password", email="user-1@example.com")

        address = other_user.addresses.create(
            address_1="10 Downing Street",
            post_code="SW1A 2AA",
            city="London",
            country_code="GB",
        )
        response = self.authenticated_client.put(
            path=reverse("user:address-detail", args=[address.id]),
            data={
                "address_1": "10 Downing Street",
                "address_2": "",
                "post_code": "test",
                "city": "10 Downing Street",
                "country_code": "GB",
            },
        )
        self.assertEqual(404, response.status_code)

    def test_detail_returns_412_on_put_if_last_modified_check_fails(self):
        address = self.user.addresses.create(
            address_1="10 Downing Street",
            post_code="SW1A 2AA",
            city="London",
            country_code="GB",
        )

        initial_response = self.authenticated_client.get(
            path=reverse("user:address-detail", args=[address.id]),
        )

        response = self.authenticated_client.put(
            path=reverse("user:address-detail", args=[address.id]),
            data={
                "address_1": "10 Downing Street",
                "address_2": "",
                "post_code": "test",
                "city": "10 Downing Street",
                "country_code": "GB",
            },
            HTTP_IF_UNMODIFIED_SINCE=initial_response.headers["Last-Modified"].replace(
                str(datetime.now().year), "1999"
            ),
        )
        self.assertEqual(412, response.status_code)

    def test_delete_actually_deletes_address_on_delete(self):
        address = self.user.addresses.create(
            address_1="10 Downing Street",
            post_code="SW1A 2AA",
            city="London",
            country_code="GB",
        )
        self.authenticated_client.delete(
            path=reverse("user:address-detail", args=[address.id]),
        )
        self.assertIsNone(UserAddressModel.objects.filter(id=address.id).first())

    def test_delete_returns_404_on_delete_if_not_address_owner(self):
        other_user = User.objects.create_user(username="user1", password="password", email="user-1@example.com")

        address = other_user.addresses.create(
            address_1="10 Downing Street",
            post_code="SW1A 2AA",
            city="London",
            country_code="GB",
        )
        response = self.authenticated_client.delete(
            path=reverse("user:address-detail", args=[address.id]),
        )
        self.assertEqual(404, response.status_code)

    def test_bulk_deletes_on_delete(self):
        addresses = [
            self.user.addresses.create(
                address_1="10 Downing Street",
                post_code="SW1A 2AA",
                city="London",
                country_code="GB",
            ),
            self.user.addresses.create(
                address_1="11 Downing Street",
                post_code="SW1A 2AA",
                city="London",
                country_code="GB",
            ),
        ]

        self.authenticated_client.delete(
            path=reverse("user:address-bulk-delete"), data={"address_ids": [address.id for address in addresses]}
        )
        self.assertEqual(0, self.user.addresses.count())

    def test_bulk_deletes_only_own_addresses_on_delete(self):
        other_user = User.objects.create_user(username="user1", password="password", email="user-1@example.com")
        addresses = [
            other_user.addresses.create(
                address_1="10 Downing Street",
                post_code="SW1A 2AA",
                city="London",
                country_code="GB",
            ),
            self.user.addresses.create(
                address_1="11 Downing Street",
                post_code="SW1A 2AA",
                city="London",
                country_code="GB",
            ),
        ]

        self.authenticated_client.delete(
            path=reverse("user:address-bulk-delete"), data={"address_ids": [address.id for address in addresses]}
        )
        self.assertIsNotNone(UserAddressModel.objects.filter(id=addresses[0].id).first())
