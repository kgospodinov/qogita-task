from rest_framework.routers import DefaultRouter

from user.views.user_address_view import UserAddressViewSet


app_name = "user"

router = DefaultRouter()
router.register(r"address", UserAddressViewSet, basename="address")

urlpatterns = router.urls
