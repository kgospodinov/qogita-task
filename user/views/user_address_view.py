from django.utils.decorators import method_decorator
from django.views.decorators.http import last_modified
from rest_framework import permissions
from rest_framework import status
from rest_framework import viewsets
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework_simplejwt.authentication import JWTAuthentication

from user.models import UserAddressModel
from user.serializers import UserAddresFilterSerializer
from user.serializers import UserAddressBulkDeleteSerializer
from user.serializers import UserAddressSerializer


def latest_entry(request, pk, *args, **kwargs):
    if user_address := UserAddressModel.objects.filter(pk=pk).first():
        return user_address.updated_at


class UserAddressViewSet(viewsets.ModelViewSet):
    authentication_classes = [JWTAuthentication]
    permission_classes = [permissions.IsAuthenticated]
    serializer_class = UserAddressSerializer

    def get_queryset(self):
        queryset = UserAddressModel.objects.filter(user_id=self.request.user.id)
        search_serializer = UserAddresFilterSerializer(data=self.request.query_params)
        search_serializer.is_valid(raise_exception=True)

        for field, field_value in search_serializer.validated_data.items():
            queryset = queryset.filter(**{f"{field}__iexact": field_value})

        return queryset

    @action(detail=False, methods=["delete"])
    def bulk_delete(self, request):
        user_address_bulk_delete = UserAddressBulkDeleteSerializer(data=request.data)
        user_address_bulk_delete.is_valid(raise_exception=True)
        self.get_queryset().filter(id__in=user_address_bulk_delete.validated_data["address_ids"]).delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    @method_decorator(last_modified(last_modified_func=latest_entry))
    def update(self, request, *args, **kwargs):
        return super().update(request, *args, **kwargs)

    @method_decorator(last_modified(last_modified_func=latest_entry))
    def retrieve(self, request, *args, **kwargs):
        return super().retrieve(request, *args, **kwargs)
